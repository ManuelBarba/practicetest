package com.wheretraining.test.models.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Account", catalog = "test")
public class Account implements Serializable {

    private Integer accountId;
    private Client client;
    private String accountNumber;
    private String accountType;
    private Double initialBalance;




    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AccountId", nullable = false)
    public Integer getAccountId() {return accountId;}

    public void setAccountId(Integer accountId) {this.accountId = accountId;}

    @ManyToOne
    @JoinColumn(name = "ClientId")
    public Client getClient() {return client;}

    public void setClient(Client client) {this.client = client;}

    @Basic
    @Column(name = "AccountNumber")
    public String getAccountNumber() {return accountNumber;}

    public void setAccountNumber(String accountNumber) {this.accountNumber = accountNumber;}

    @Basic
    @Column(name = "AccountType")
    public String getAccountType() {return accountType;}

    public void setAccountType(String accountType) {this.accountType = accountType;}

    @Basic
    @Column(name = "InitialBalance")
    public Double getInitialBalance() {return initialBalance;}

    public void setInitialBalance(Double initialBalance) {this.initialBalance = initialBalance;}
}
