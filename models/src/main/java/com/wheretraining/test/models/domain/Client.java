package com.wheretraining.test.models.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Client", catalog = "test")
public class Client implements Serializable {

    private Integer clientId;
    private Person person;
    private String contrasena;
    private String estado;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ClientId", nullable = false)
    public Integer getClientId() {return clientId;}

    public void setClientId(Integer clientId) {this.clientId = clientId;}

    @ManyToOne
    @JoinColumn(name ="PersonId")
    public Person getPerson() {return person;}

    public void setPerson(Person person) {this.person = person;}

    @Basic
    @Column(name = "Contrasena")
    public String getContrasena() {return contrasena;}

    public void setContrasena(String contrasena) {this.contrasena = contrasena;}

    @Basic
    @Column(name = "Estado")
    public String getEstado() {return estado;}

    public void setEstado(String estado) {this.estado = estado;}
}
