package com.wheretraining.test.models;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@EnableAutoConfiguration
public class ModelsMain {

    public static void main(String[] args){
        SpringApplication.run(ModelsMain.class, args);
    }

}
