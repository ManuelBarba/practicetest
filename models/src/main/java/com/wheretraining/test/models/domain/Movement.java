package com.wheretraining.test.models.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "Movement", catalog = "test")
public class Movement implements Serializable {

    private Integer movementId;
    private Account account;
    private Timestamp movementDate;
    private String movementType;
    private Double value;
    private Double balance;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MovementId", nullable = false)
    public Integer getMovementId() {return movementId;}

    public void setMovementId(Integer movementId) {this.movementId = movementId;}

    @ManyToOne
    @JoinColumn(name = "AccountId")
    public Account getAccount() {return account;}

    public void setAccount(Account account) {this.account = account;}

    @Basic
    @Column(name = "MovementDate")
    public Timestamp getMovementDate() {return movementDate;}

    public void setMovementDate(Timestamp movementDate) {this.movementDate = movementDate;}

    @Basic
    @Column(name = "MovementType")
    public String getMovementType() {return movementType;}

    public void setMovementType(String movementType) {this.movementType = movementType;}

    @Basic
    @Column(name = "Value")
    public Double getValue() {return value;}

    public void setValue(Double value) {this.value = value;}

    @Basic
    @Column(name = "Balance")
    public Double getBalance() {return balance;}

    public void setBalance(Double balance) {this.balance = balance;}
}
