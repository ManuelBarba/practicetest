package com.wheretraining.test.models.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Person", catalog = "test")
public class Person implements Serializable {

    private Integer personId;
    private String name;
    private String gender;
    private Integer age;
    private String id;
    private String address;
    private String phoneNumber;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PersonId", nullable = false)
    public Integer getPersonId() {return personId;}

    public void setPersonId(Integer personId) {this.personId = personId;}

    @Basic
    @Column(name = "Name")
    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    @Basic
    @Column(name = "Gender")
    public String getGender() {return gender;}

    public void setGender(String gender) {this.gender = gender;}

    @Basic
    @Column(name = "Age")
    public Integer getAge() {return age;}

    public void setAge(Integer age) {this.age = age;}

    @Basic
    @Column(name = "Id")
    public String getId() {return id;}

    public void setId(String id) {this.id = id;}

    @Basic
    @Column(name = "Address")
    public String getAddress() {return address;}

    public void setAddress(String address) {this.address = address;}

    @Basic
    @Column(name = "PhoneNumber")
    public String getPhoneNumber() {return phoneNumber;}

    public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}
}
