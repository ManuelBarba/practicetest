package com.wheretraining.test.domain.controllers;

import com.wheretraining.test.domain.repositories.PersonRepository;
import com.wheretraining.test.models.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Transactional
@RequestMapping(path = "/domain/person")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;

    @GetMapping
    List<Person> findAll(){ return personRepository.findAll(); }

    @PostMapping
    Person create(@RequestBody Person o){ return personRepository.save(o);}

    @PutMapping
    Person update(@RequestBody Person o){ return personRepository.save(o);}

    @DeleteMapping(path = "/delete/{id}")
    void delete(@PathVariable Integer id){ personRepository.deleteById(id);}
}
