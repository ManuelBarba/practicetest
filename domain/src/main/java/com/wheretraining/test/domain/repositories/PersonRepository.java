package com.wheretraining.test.domain.repositories;

import com.wheretraining.test.models.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Integer> {

}
