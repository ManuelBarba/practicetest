package com.wheretraining.test.domain.controllers;

import com.wheretraining.test.domain.repositories.ClientRepository;
import com.wheretraining.test.domain.repositories.PersonRepository;
import com.wheretraining.test.models.domain.Client;
import com.wheretraining.test.models.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Transactional
@RequestMapping(path = "/domain/client")
public class ClientController {

    @Autowired
    private ClientRepository clientRepository;

    @GetMapping
    List<Client> findAll(){ return clientRepository.findAll(); }
    @GetMapping(path = "/identification/{id}")
    Client create(@PathVariable String id){ return clientRepository.findByPerson_Id(id);}

    @PostMapping
    Client create(@RequestBody Client o){ return clientRepository.save(o);}

    @PutMapping
    Client update(@RequestBody Client o){ return clientRepository.save(o);}

    @DeleteMapping
    void delete(@RequestBody Client o){clientRepository.delete(o);}
}
