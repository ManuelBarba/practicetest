package com.wheretraining.test.domain.controllers;

import com.wheretraining.test.domain.repositories.AccountRepository;
import com.wheretraining.test.domain.repositories.PersonRepository;
import com.wheretraining.test.models.domain.Account;
import com.wheretraining.test.models.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Transactional
@RequestMapping(path = "/domain/account")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping
    List<Account> findAll(){ return accountRepository.findAll(); }

    @GetMapping(path = "/number/{number}")
    Account findAll(@PathVariable String number){ return accountRepository.findByAccountNumber(number); }

    @GetMapping(path = "/client/identification/{number}")
    List<Account> findByClientIdentification(@PathVariable String number){return accountRepository.findByClient_Person_Id(number);}

    @PostMapping
    Account create(@RequestBody Account o){ return accountRepository.save(o);}

    @PutMapping
    Account update(@RequestBody Account o){ return accountRepository.save(o);}

    @DeleteMapping
    void delete(@RequestBody Account o){ accountRepository.delete(o);}
}
