package com.wheretraining.test.domain.controllers;

import com.wheretraining.test.domain.repositories.MovementRepository;
import com.wheretraining.test.domain.repositories.PersonRepository;
import com.wheretraining.test.models.domain.Movement;
import com.wheretraining.test.models.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Transactional
@RequestMapping(path = "/domain/movement")
public class MovementController {

    @Autowired
    private MovementRepository movementRepository;

    @GetMapping
    List<Movement> findAll(){ return movementRepository.findAll(); }

    @GetMapping(path = "/account/{accountId}")
    List<Movement> findAll(@PathVariable Integer accountId){ return movementRepository.findByAccount_AccountId(accountId); }

    @PostMapping
    Movement create(@RequestBody Movement o){ return movementRepository.save(o);}

    @PutMapping
    Movement update(@RequestBody Movement o){ return movementRepository.save(o);}

    @DeleteMapping
    void delete(@RequestBody Movement o){ movementRepository.delete(o);}
}
