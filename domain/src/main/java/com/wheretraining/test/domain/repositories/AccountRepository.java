package com.wheretraining.test.domain.repositories;

import com.wheretraining.test.models.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Integer> {

    Account findByAccountNumber(String accountNumber);

    List<Account> findByClient_Person_Id(String id);

}
