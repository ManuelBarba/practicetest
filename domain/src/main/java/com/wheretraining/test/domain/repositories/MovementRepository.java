package com.wheretraining.test.domain.repositories;

import com.wheretraining.test.models.domain.Movement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MovementRepository extends JpaRepository<Movement, Integer> {

    List<Movement> findByAccount_AccountId(Integer idAccount);

}
