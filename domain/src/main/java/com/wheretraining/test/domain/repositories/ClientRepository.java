package com.wheretraining.test.domain.repositories;

import com.wheretraining.test.models.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Integer>  {

    Client findByPerson_Id(String id);

}
