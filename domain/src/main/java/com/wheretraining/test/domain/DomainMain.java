package com.wheretraining.test.domain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan(basePackages = "com.wheretraining.test.models")
@SpringBootApplication(scanBasePackages = {"com.wheretraining.test.domain"})
public class DomainMain {

    public static void main(String[] args){
        SpringApplication.run(DomainMain.class, args);
    }

}
