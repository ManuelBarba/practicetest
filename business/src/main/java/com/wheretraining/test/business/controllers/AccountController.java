package com.wheretraining.test.business.controllers;

import com.wheretraining.test.business.utilities.GeneralUtilitiesApl;
import com.wheretraining.test.models.business.Account;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.wheretraining.test.business.utilities.GeneralUtilitiesApl.UrlDominio;

@RestController
@RequestMapping(path = "/business/account")
public class AccountController {

    private String urlDomain = UrlDominio + "account/";


    @GetMapping
    List<Account> findAll(){
        return Arrays.asList(Objects.requireNonNull(GeneralUtilitiesApl.restTemplate.getForObject(urlDomain, Account[].class)));
    }

    @PostMapping
    Account create(@RequestBody Account o){
        return GeneralUtilitiesApl.restTemplate.postForObject(urlDomain ,o , Account.class);
    }

    @GetMapping(path = "/client/identification/{id}")
    List<Account> findByClientIdentification(@PathVariable String id){
        return Arrays.asList(Objects.requireNonNull(GeneralUtilitiesApl.restTemplate.getForObject(urlDomain + "client/identification/" + id, Account[].class)));
    }

    @GetMapping(path = "/number/{number}")
    Account findByAccountNumber(@PathVariable String number){
        return GeneralUtilitiesApl.restTemplate.getForObject(urlDomain + "number/" + number, Account.class);
    }

    @PutMapping
    Account update(@RequestBody Account o){
        return GeneralUtilitiesApl.restTemplate.postForObject(urlDomain ,o , Account.class);
    }

    @DeleteMapping
    void delete(@RequestBody Account o){
        GeneralUtilitiesApl.restTemplate.delete(urlDomain ,o );
    }

}
