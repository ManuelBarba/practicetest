package com.wheretraining.test.business.controllers;

import com.wheretraining.test.business.exceptions.BusinessException;
import com.wheretraining.test.business.responseObjects.MovementPerUser;
import com.wheretraining.test.business.utilities.GeneralUtilitiesApl;
import com.wheretraining.test.models.business.Account;
import com.wheretraining.test.models.business.Movement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

import static com.wheretraining.test.business.constantes.constantes.RETIRO;
import static com.wheretraining.test.business.constantes.exceptions.*;
import static com.wheretraining.test.business.utilities.GeneralUtilitiesApl.UrlDominio;

@RestController
@RequestMapping(path = "/business/movement")
public class MovementController {

    private final String urlDomain = UrlDominio + "movement/";

    @Autowired
    AccountController accountController;
    @Autowired
    ClientController clientController;


    @GetMapping
    List<Movement> findAll(){
        return Arrays.asList(Objects.requireNonNull(GeneralUtilitiesApl.restTemplate.getForObject(urlDomain, Movement[].class)));
    }

    @GetMapping(path = "/account/{accountId}")
    List<Movement> findAllByAccountId(@PathVariable Integer accountId){
        return Arrays.asList(Objects.requireNonNull(GeneralUtilitiesApl.restTemplate.getForObject(urlDomain + "account/" + accountId, Movement[].class)));
    }

    @PostMapping
    Movement create(@RequestBody Movement o){
        return GeneralUtilitiesApl.restTemplate.postForObject(urlDomain ,o , Movement.class);
    }

    @PostMapping(path = "/newMovement")
    Movement insertNewMovement(@RequestBody Movement movement) throws BusinessException {
        Account affectedAccount = movement.getAccount();

        if(affectedAccount == null){
            throw new BusinessException(ACCOUNTNOTFOUNDEXCEPTION);
        }

        List<Movement> movements = findAllByAccountId(affectedAccount.getAccountId());

        if(!movements.isEmpty()){
            Movement lastMovement = movements.stream().max(Comparator.comparing(Movement::getMovementId)).get();
            validateMovementType(movement, lastMovement.getBalance());
        }else{
            validateMovementType(movement, affectedAccount.getInitialBalance());
        }

        return create(movement);
    }

    @GetMapping(path = "/client/identification/{id}")
    List<MovementPerUser> findAllMovementsByClient(@PathVariable String id) throws BusinessException {
        List<Account> account = accountController.findByClientIdentification(id);
        List<MovementPerUser> mpuList = new ArrayList<>();

        account.forEach(a -> {
            List<Movement> movements = findAllByAccountId(a.getAccountId());
            try {
                createMPUReturningData(a, movements, mpuList);
            } catch (BusinessException e) {
                throw new RuntimeException(e);
            }

        });

        return mpuList;
    }

    @PutMapping
    Movement update(@RequestBody Movement o){
        return GeneralUtilitiesApl.restTemplate.postForObject(urlDomain ,o , Movement.class);
    }

    @DeleteMapping
    void delete(@RequestBody Movement o){
        GeneralUtilitiesApl.restTemplate.delete(urlDomain ,o );
    }

    private void validateMovementType(Movement movement, Double balance) throws BusinessException {
        if(movement.getMovementType().equals(RETIRO)){
            movement.setBalance(calculateNewBalance(balance, movement.getValue()));
        }else{
            movement.setBalance(balance + movement.getValue());
        }
    }

    private Double calculateNewBalance(Double x, Double y) throws BusinessException {
        Double operation = x - y;
        if(operation < 0){
            throw new BusinessException(LACKOFBALANCEEXCEPTION);
        }
        else return operation;
    }

    List<MovementPerUser> createMPUReturningData(Account account, List<Movement> movements, List<MovementPerUser> mpuList) throws BusinessException {
        if(movements.isEmpty())   {
            throw new BusinessException(MOVEMENTSNOTFOUNDEXCEPTION);
        }

        movements.forEach(m -> {
            MovementPerUser mpu = new MovementPerUser();
            mpu.setDate(m.getMovementDate());
            mpu.setClient(account.getClient().getPerson().getName());
            mpu.setAccountNumber(account.getAccountNumber());
            mpu.setAccountType(account.getAccountType());
            mpu.setInitialBalance(account.getInitialBalance());
            mpu.setStatus(account.getClient().getEstado());
            mpu.setMovement(m.getValue());
            mpu.setAvailableBalance(m.getBalance());

            mpuList.add(mpu);
        });

        return mpuList.stream().sorted(Comparator.comparing(MovementPerUser::getDate).reversed()).collect(Collectors.toList());
    }
}
