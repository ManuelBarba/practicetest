package com.wheretraining.test.business.constantes;

public class exceptions {

    public static final String ACCOUNTNOTFOUNDEXCEPTION = "No fue encontrada una cuenta con este numero";
    public static final String LACKOFBALANCEEXCEPTION = "No se puede realizar el retiro debido a falta de saldo";

    public static final String CLIENTNOTFOUNDEXCEPTION = "No fue encontrada un cliente con este numero de identificación";

    public static final String MOVEMENTSNOTFOUNDEXCEPTION = "No fueron encontrados movimientos para ese cliente";



}
