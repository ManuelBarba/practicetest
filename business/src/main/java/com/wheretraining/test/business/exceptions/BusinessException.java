package com.wheretraining.test.business.exceptions;

public class BusinessException extends Exception{
    public BusinessException(String errorMessage) {
        super(errorMessage);
    }
}
