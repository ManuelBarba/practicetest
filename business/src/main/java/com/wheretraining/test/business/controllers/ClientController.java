package com.wheretraining.test.business.controllers;

import com.wheretraining.test.business.utilities.GeneralUtilitiesApl;
import com.wheretraining.test.models.business.Account;
import com.wheretraining.test.models.business.Client;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.wheretraining.test.business.utilities.GeneralUtilitiesApl.UrlDominio;

@RestController
@RequestMapping(path = "/business/client")
public class ClientController {

    private String urlDomain = UrlDominio + "client/";


    @GetMapping
    List<Client> findAll(){
        return Arrays.asList(Objects.requireNonNull(GeneralUtilitiesApl.restTemplate.getForObject(urlDomain, Client[].class)));
    }

    @PostMapping
    Client create(@RequestBody Client o){
        return GeneralUtilitiesApl.restTemplate.postForObject(urlDomain ,o , Client.class);
    }
    @GetMapping(path = "/identification/{id}")
    Client findByPersonIdentification(@PathVariable String id){
        return GeneralUtilitiesApl.restTemplate.getForObject(urlDomain + "identification/" + id, Client.class);
    }

    @PutMapping
    Client update(@RequestBody Client o){
        return GeneralUtilitiesApl.restTemplate.postForObject(urlDomain ,o , Client.class);
    }

    @DeleteMapping
    void delete(@RequestBody Client o){
        GeneralUtilitiesApl.restTemplate.delete(urlDomain ,o );
    }

}
