package com.wheretraining.test.business.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class GeneralUtilitiesApl {

    public static RestTemplate restTemplate = new RestTemplate();
    public static final String UrlDominio = "http://localhost:7000/domain/";
    @Autowired
    public static void setRestTemplate(RestTemplate restTemplate) { GeneralUtilitiesApl.restTemplate = restTemplate; }
}
