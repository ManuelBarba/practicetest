package com.wheretraining.test.business;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan(basePackages = "com.wheretraining.test.models")
@SpringBootApplication(scanBasePackages = {"com.wheretraining.test.business"})
public class BusinessMain {

    public static void main(String[] args){
        SpringApplication.run(BusinessMain.class, args);
    }

}
