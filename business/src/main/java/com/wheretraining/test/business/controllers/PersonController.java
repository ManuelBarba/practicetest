package com.wheretraining.test.business.controllers;

import com.wheretraining.test.business.utilities.GeneralUtilitiesApl;
import com.wheretraining.test.models.business.Person;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.wheretraining.test.business.utilities.GeneralUtilitiesApl.UrlDominio;

@RestController
@RequestMapping(path = "/business/person")
public class PersonController {

    private String urlDomain = UrlDominio + "person/";


    @GetMapping
    List<Person> findAll(){
        return Arrays.asList(Objects.requireNonNull(GeneralUtilitiesApl.restTemplate.getForObject(urlDomain, Person[].class)));
    }

    @PostMapping
    Person create(@RequestBody Person o){
        return GeneralUtilitiesApl.restTemplate.postForObject(urlDomain ,o , Person.class);
    }

    @PutMapping
    Person update(@RequestBody Person o){
        return GeneralUtilitiesApl.restTemplate.postForObject(urlDomain ,o , Person.class);
    }
    @DeleteMapping(path = "/delete/{id}")
    void delete(@PathVariable Integer id){
        GeneralUtilitiesApl.restTemplate.delete(urlDomain + "delete/" + id, Void.class);
    }

}
