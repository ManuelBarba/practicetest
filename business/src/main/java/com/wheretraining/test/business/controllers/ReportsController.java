package com.wheretraining.test.business.controllers;

import com.wheretraining.test.business.exceptions.BusinessException;
import com.wheretraining.test.business.responseObjects.MovementPerUser;
import com.wheretraining.test.models.business.Account;
import com.wheretraining.test.models.business.Movement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static com.wheretraining.test.business.constantes.exceptions.ACCOUNTNOTFOUNDEXCEPTION;
import static com.wheretraining.test.business.constantes.exceptions.MOVEMENTSNOTFOUNDEXCEPTION;

@RestController
@RequestMapping(path = "/business/report")
public class ReportsController {

    @Autowired
    AccountController accountController;
    @Autowired
    ClientController clientController;
    @Autowired
    MovementController movementController;


    @GetMapping(path = "/client/identification/{id}/dateFrom/{dateFrom}/dateTo/{dateTo}")
    List<MovementPerUser> findAllMovementsByClient(@PathVariable String id,@PathVariable String dateFrom, @PathVariable String dateTo) throws BusinessException {
        List<Account> account = accountController.findByClientIdentification(id);
        AtomicReference<List<MovementPerUser>> movementPerUserList = new AtomicReference();

        if(account.isEmpty()){
            throw new BusinessException(ACCOUNTNOTFOUNDEXCEPTION);
        }

        account.forEach(a -> {
            List<Movement> movements = movementController.findAllByAccountId(a.getAccountId());
            try {
                movementPerUserList.set(createMPUReturningDataByDate(a, movements, dateFrom, dateTo));
            } catch (BusinessException | ParseException e) {
                throw new RuntimeException(e);
            }

        });

        return movementPerUserList.get();
    }

    List<MovementPerUser> createMPUReturningDataByDate(Account account, List<Movement> movements, String dateFrom, String dateTo) throws BusinessException, ParseException {
        List<MovementPerUser> mpuList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parsedDateFrom = dateFormat.parse(dateFrom);
        Date parsedDateTo = dateFormat.parse(dateTo);
        Timestamp timestampFrom = new java.sql.Timestamp(parsedDateFrom.getTime());
        Timestamp timestampTo = new java.sql.Timestamp(parsedDateTo.getTime());

        movements.forEach(m -> {
            MovementPerUser mpu = new MovementPerUser();
            mpu.setDate(m.getMovementDate());
            mpu.setClient(account.getClient().getPerson().getName());
            mpu.setAccountNumber(account.getAccountNumber());
            mpu.setAccountType(account.getAccountType());
            mpu.setInitialBalance(account.getInitialBalance());
            mpu.setStatus(account.getClient().getEstado());
            mpu.setMovement(m.getValue());
            mpu.setAvailableBalance(m.getBalance());

            mpuList.add(mpu);
        });

        List<MovementPerUser> movementPerUserList = mpuList.stream().filter(mpu ->
                mpu.getDate().after(timestampFrom) && mpu.getDate().before(timestampTo))
                .collect(Collectors.toList());

        return movementPerUserList;
    }

}
