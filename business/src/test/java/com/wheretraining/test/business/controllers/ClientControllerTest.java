package com.wheretraining.test.business.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@WebAppConfiguration
class ClientControllerTest {

    private final static String CLIENT_URL = "http://localhost:8000/business/client";
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }


    @Test
    void findAll() throws Exception {
        MvcResult findAll = mockMvc.perform(MockMvcRequestBuilders.get(CLIENT_URL))
                .andReturn();

        assertEquals(200, findAll.getResponse().getStatus());
    }

    @Test
    void findByPersonIdentification() throws Exception {
        MvcResult findAll = mockMvc.perform(MockMvcRequestBuilders.get(CLIENT_URL + "/identification/4564556"))
                .andReturn();

        assertEquals(200, findAll.getResponse().getStatus());
    }
}