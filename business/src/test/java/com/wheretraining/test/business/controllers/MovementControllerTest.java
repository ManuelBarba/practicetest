package com.wheretraining.test.business.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wheretraining.test.models.business.Movement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@WebAppConfiguration
class MovementControllerTest {

    private final static String MOVEMENT_URL = "http://localhost:8000/business/movement";

    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void insertNewMovement() throws Exception {
        assertThrows(NestedServletException.class, () -> {
            this.mockMvc.perform(MockMvcRequestBuilders.post(MOVEMENT_URL + "/newMovement")
                            .content(mapToJson(buildMovement()))
                            .contentType(MediaType.APPLICATION_JSON_VALUE))
                    .andReturn();
        });
    }

    @Test
    void findAllMovementsByClient() throws Exception {
        MvcResult movementsByClient = mockMvc.perform(MockMvcRequestBuilders.get(MOVEMENT_URL + "/client/identification/4564556"))
                .andReturn();

        assertEquals(200, movementsByClient.getResponse().getStatus());
    }

    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }
    private Movement buildMovement(){
        Movement movement = new Movement();
        movement.setMovementDate(new Timestamp(System.currentTimeMillis()));
        movement.setBalance(1200.0);
        movement.setValue(100.0);
        movement.setMovementType("Retiro");
        movement.setAccount(null);

        return movement;
    }

}