package com.wheretraining.test.business.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@WebAppConfiguration
class AccountControllerTest {

    private final static String MOVEMENT_URL = "http://localhost:8000/business/account";
    MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void findByClientIdentification() throws Exception {
        MvcResult clientIdentification = mockMvc.perform(MockMvcRequestBuilders.get(MOVEMENT_URL + "/client/identification/4564556"))
                .andReturn();

        assertEquals(200, clientIdentification.getResponse().getStatus());
    }

    @Test
    void findByAccountNumber() throws Exception {
        MvcResult accountNumber = mockMvc.perform(MockMvcRequestBuilders.get(MOVEMENT_URL + "/number/478758"))
                .andReturn();

        assertEquals(200, accountNumber.getResponse().getStatus());
    }
}